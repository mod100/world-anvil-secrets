/**
 * Store used css classes sued in this module
 * Some are retrieved from main module. Others are already set.
 * @enum {string}
 */
const customCss = {
    section: "",
    publicSection: "",
    coreSecretSection: "",
    secretSection: "wa-secret",
    hiddenSuffix: "hidden",
    revealedSuffix: "revealed"
};

/**
 * Retrieve some of the Css classes that are used in the core module.
 * Only done once.
 */
function retrieveBaseCssClasses() {
    if( customCss.section ) { return; } // Already done
    const waModule = game.modules.get("world-anvil");
    customCss.section = waModule.api.ARTICLE_CSS_CLASSES.ALL_PARTS;
    customCss.publicSection = waModule.api.ARTICLE_CSS_CLASSES.PUBLIC_SECTION;
    customCss.coreSecretSection = waModule.api.ARTICLE_CSS_CLASSES.SECRET_SECTION;
}


/**
 * FIXME : Copyed from main module. will need to wait for v 1.4.0 for removing it.
 * (Not yet public in core module)
 */
function formatArticleContent(content) {

  // Disable image source attributes so that they do not begin loading immediately
  content = content.replace(/src=/g, "data-src=");

  // HTML formatting
  const div = document.createElement("div");
  div.innerHTML = content;

  // Paragraph Breaks
  const t = document.createTextNode("%p%");
  div.querySelectorAll("span.line-spacer").forEach(s => s.parentElement.replaceChild(t.cloneNode(), s));

  // Image from body
  div.querySelectorAll("img").forEach(i => {

    // Default href link to hosted foundry server, and not WA. => it needs to be set
    i.parentElement.href = `https://worldanvil.com/${i.parentElement.pathname}`;

    // Set image source
    let img = new Image();
    img.src = `https://worldanvil.com${i.dataset.src}`;
    delete i.dataset.src;
    img.alt = i.alt;
    img.title = i.title;
    img.style.cssText = i.style.cssText; //Retain custum img size
    i.parentElement.replaceChild(img, i);
  });

  // World Anvil Content Links
  div.querySelectorAll('span[data-article-id]').forEach(el => {
    el.classList.add("entity-link", "wa-link");
  });
  div.querySelectorAll('a[data-article-id]').forEach(el => {
    el.classList.add("entity-link", "wa-link");
    const span = document.createElement("span");
    span.classList = el.classList;
    Object.entries(el.dataset).forEach(e => span.dataset[e[0]] = e[1]);
    span.textContent = el.textContent;
    el.replaceWith(span);
  });

  // Regex formatting
  let html = div.innerHTML;
  html = html.replace(/%p%/g, "</p>\n<p>");

  return html;
}


/* -------------------------------------------- */
/*   Type Definitions  (Reminder)               */
/* -------------------------------------------- */

/**
 * @typedef {Object} ParsedArticleResult    Used by the hook WAParseArticle. It contains primary data which could be altered by additional modules
 * @property {string} html                  What will become the journal entry content. Is in html format
 * @property {Image} img                    What will become the journal entry image.
 * @property {object} waFlags               Journal entry flags which will be store inside entry.data.flags["world-anvil"]
 */


/* -------------------------------------------- */
/*   Called from hooks                          */
/* -------------------------------------------- */

/**
 * WA Secret are parse differently: Each <h3> in the parsed content is considerated as a distinct secret
 * @param {object} article Article, as retrieved from WA
 * @param {ParsedArticleResult} result See the type definition
 */
export function parseArticle( article, result ) {

  // Make sure css values are correctly set
  retrieveBaseCssClasses();

  const template = document.createElement('template');
  template.innerHTML = result.html;

  // Removing old secrets sections
  for( let child of template.content.childNodes ) {
    if( child.className.includes(customCss.coreSecretSection) ) {
      child.remove();
    }
  }
  
  // Parsing article and retrieving necesseray data to add secrets at the bottom of the journal entry
  const secretSectionIds = ["seeded"];
  const secretEntries = Object.entries(article.sections ?? {}).filter( ([id, section]) => {
    return secretSectionIds.includes(id) ;
  });
  result.waFlags.hasSecrets = secretEntries.length > 0;
  result.waFlags.secrets = {};


  // Adding sections on bottom of the journal entry
  for ( let [id, section] of secretEntries ) {

    // Parse section. Each paragraph with a <h3> is handled as a separated secret
    const startsWithH3 = section.contentParsed.startsWith('<h3');
    const secrets = section.contentParsed.split('<h3');
    secrets.forEach( (secret, index) => {

      if( secret.trim() === "" ) return; // If h3 are used, fist element is often empty
      const secretId = id + "-" + index;
      result.waFlags.secrets[ secretId ] = false;

      // Each secret is referenced in the secrets flag.
      const htmlSection = document.createElement("section");
      htmlSection.classList.add(customCss.section, customCss.secretSection);
      htmlSection.setAttribute("data-section-id", secretId);

      let content = secret;
      if( index != 0 || startsWithH3 ) { content = '<h3' + content; }

      htmlSection.innerHTML = formatArticleContent(content);

      template.content.append(htmlSection)
    });
  }
  result.html = template.innerHTML;

}

/**
 * When updating an existing journal Entry, we need to retrieve old values for all existing secrets
 * @param {object} entry Current journal entry which should be updated
 * @param {object} newFlags See ParsedArticleResult.waFlags
 */
export function updateCurrentJournalEntryFlags(entry, newFlags) {

  // Update waFlag with already shared secrets
  const oldFlags = entry.data.flags["world-anvil"]?.secrets;
  if( oldFlags ) {
    for( let [secretId, secretValue] of Object.entries(oldFlags) ) {
      if( newFlags.secrets.hasOwnProperty(secretId) ) { // Do not add unknwon secrets reference here
        newFlags.secrets[secretId] = secretValue;
      }
    }
  };
}

/**
 * Secret sections have a different display wether the user is a GM or not.
 * GMs see all secrets and can toggle these secrets for other players
 * @param {object} sheet A rendered sheet. May be a character or a journal sheet
 * @param {object} html Its builded display
 */
export function renderSecretSections(sheet, html) {

  // Make sure css values are correctly set
  retrieveBaseCssClasses();

  // Handle entity secrets
  const entity = sheet.object;
  const secrets = entity.getFlag("world-anvil", "secrets");
  const cssSecrets = "." + customCss.section + "." + customCss.secretSection;
  const htmlSecrets = html.find(cssSecrets);
  for( let htmlSecret of htmlSecrets ) {
    const secretId = htmlSecret.dataset.sectionId;
    const revealed = secrets[secretId] ?? false;
    if ( game.user.isGM ) { // Not the same treatment for GM who sees all secret and other players
      htmlSecret.classList.add( revealed ? customCss.revealedSuffix : customCss.hiddenSuffix );

    } else { // For players
      if( revealed ) {
        htmlSecret.classList.replace(customCss.secretSection, customCss.publicSection);
      } else {
        htmlSecret.remove();
      }
    }
  }
}

/**
 * Add listeners for toggling secret displays
 * @param {object} sheet A rendered sheet. May be a character or a journal sheet
 * @param {object} html Its builded display
 */
export function addToggleSecretsListeners(sheet, html) {
  
  // Retrieve related journal Entry if it still exist
  const entity = sheet.object;
  const articleId = entity.getFlag("world-anvil", "articleId");
  const journalEntry = game.journal.find(e => e.getFlag("world-anvil", "articleId") === articleId);
  const target = journalEntry ?? entity; // If the Journal entry is still present, modification will be transfered to other entities after update

  // Convenience function
  const toggleSecret = async (event, newValue) => {
    const secretId = event.currentTarget.dataset.sectionId;
    await target.setFlag("world-anvil", "secrets." + secretId, newValue);
    target.render(false);

    const module = game.modules.get("world-anvil");
    module.browser.render(false);
  };

  const cssSecrets = "." + customCss.section + "." + customCss.secretSection;

  const htmlReveleadSecrets = html.find(cssSecrets + "." + customCss.revealedSuffix);
  htmlReveleadSecrets.click( event => toggleSecret(event, false) );

  const htmlHiddenSecrets = html.find(cssSecrets + "." + customCss.hiddenSuffix);
  htmlHiddenSecrets.click( event => toggleSecret(event, true) );
}
  