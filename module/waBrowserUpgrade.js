export function addSecretButtonsInBrower(html) {

    const articles = html.find(".article");
    for( let index = 0; index < articles.length; index++ ) {
        const article = articles[index];
        const articleId = article.dataset.articleId;
        const entryId = article.dataset.entryId;
        if( !articleId || !entryId ) { continue; } // Not yet imported

        const entry = game.journal.get(entryId);
        const hasSecrets = entry?.getFlag("world-anvil", "hasSecrets") ?? false;
        if( !hasSecrets ) { continue; } // This article doesn't have any secret

        const secrets = entry.getFlag("world-anvil", "secrets");
        const someSecretsAreHidden = Object.values(secrets).some( v => !v );

        // Creating additional button for the toggling all secrets
        const secretButton = document.createElement("button");
        secretButton.type = "button";
        secretButton.classList.add('world-anvil-control', 'has-secret');
        secretButton.dataset.entryId = "" + entryId;

        if( someSecretsAreHidden ) {
            secretButton.title = game.i18n.localize("WASecret.ButtonSecretReveal");
            secretButton.classList.add("reveal-secrets");
            secretButton.innerHTML = '<i class="fas fa-lock"></i>';

        } else {
            secretButton.title = game.i18n.localize("WASecret.ButtonSecretHide");
            secretButton.classList.add("hide-secrets");
            secretButton.innerHTML = '<i class="fas fa-lock-open"></i>';
        }
  
        // Add it to the control line
        const controls = article.getElementsByClassName("controls")[0];
        controls.prepend(secretButton);
        secretButton.outerHTML += '\n';
    }
}

/**
 * Reval all secrets related to an article
 * @param {object} event Html clic event
 */
export async function revealSecrets(event, app) {
    const button = event.currentTarget;
    const entryId = button.closest(".article").dataset.entryId

    const entry = game.journal.find(j => j.id === entryId) ?? null;
    if( !entry ) { throw 'Can\'t find journal entry with id : ' + entryId; }

    const newSecretFlag = {};
    for( let secretId of Object.keys( entry.getFlag("world-anvil", "secrets") ) ) {
        newSecretFlag[secretId] = true;
    }

    await entry.setFlag("world-anvil", "secrets", newSecretFlag);
    app.render();
}

/* -------------------------------------------- */

/**
 * Hide all secrets related to an article
 * @param {object} event Html clic event
 */
export async function hideSecrets(event, app) {
    const button = event.currentTarget;
    const entryId = button.closest(".article").dataset.entryId

    const entry = game.journal.find(j => j.id === entryId) ?? null;
    if( !entry ) { throw 'Can\'t find journal entry with id : ' + entryId; }

    const newSecretFlag = {};
    for( let secretId of Object.keys( entry.getFlag("world-anvil", "secrets") ) ) {
        newSecretFlag[secretId] = false;
    }

    await entry.setFlag("world-anvil", "secrets", newSecretFlag);
    app.render();
}
