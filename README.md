# Foundry Virtual Tabletop - World Anvil Integration - Custom secret management

This module provides a custom way to handle secrets coming from [World Anvil](https://worldanvil.com) articles.

It comes on top of the core module [World Anvil Integration](https://gitlab.com/foundrynet/world-anvil/-/raw/master/module.json). It needs at least the version 1.3.0 of the core module.

-----

## Installation

This module can be installed by using the following module manifest url: (https://gitlab.com/adrien.schiehle/world-anvil-secrets/-/raw/main/module.json).

-----

## Features

### Parsing secrets

When importing a World-Anvil article, the `Storyteller Seeds` part is parsed differently. Each `[h2]` marks the start of a new secret inside the seeds.

In this example, two secrets are detected :
![Parsing secrets](docs/README_storyteller_seeds.png?raw=true)

### Toggling secrets

Once it has been imported, each secret can be shered with players separately. 

You can do so by opening the Journal Entry:
- Green opended lock means the secret is visible by your players
- Red closed lock means the secret is hidden from your players

![Toggling secrets](docs/README_toggling_secrets.png?raw=true)


You can also hide or display all secrets directly from the WorldAnvilBrowser :

![Toggling secrets](docs/README_toggling_all_secrets.png?raw=true)

-----

## Still not here

### Secret section from World Anvil

For now, the secret section is not available in the API. Meaning it can't be shared with this Foundry module :

![Secret section](docs/README_real_secrets_ko.png?raw=true)

I will add them once they are available.

-----

