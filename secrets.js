import * as browserUpgrade from './module/waBrowserUpgrade.js';
import * as journalUpgrade from './module/journalEntryUpgrade.js';

/**
 * WA Secret are parse differently: Each <h3> in the parsed content is considerated as a distinct secret
 */
Hooks.on("WAParseArticle", (article, result) => {
  journalUpgrade.parseArticle( article, result );
});


/**
 * When updating an existing journal Entry, we need to retrieve old values for all existing secrets
 */
 Hooks.on("WAUpdateJournalEntry", (entry, content) => {
  journalUpgrade.updateCurrentJournalEntryFlags(entry, content.waFlags);
});

/**
 * Augment rendered Journal sheets to add secret sections management
 */
Hooks.on("renderJournalSheet", (app, html, data) => {

  // Only for journal entries linked to a WA article
  if( ! app.object.getFlag("world-anvil", "articleId") ) return;

  journalUpgrade.renderSecretSections(app, html);
  if( game.user.isGM ) {
    journalUpgrade.addToggleSecretsListeners(app, html);
  }
});

/**
 * Augment rendered WorldAnvilBrowser sheet to modify secret button
 */
Hooks.on("renderWorldAnvilBrowser", (app, html, data) => {

	// Update html content by substituting has-secret buttons
  browserUpgrade.addSecretButtonsInBrower(html);

  // Add listener for this buttons
  html.find(".world-anvil-control.has-secret.reveal-secrets").click( event => browserUpgrade.revealSecrets(event, app) );
  html.find(".world-anvil-control.has-secret.hide-secrets").click( event => browserUpgrade.hideSecrets(event, app) );

});

//----------------------------------
// Also do the same thing one Actor sheet 
// in case world-module-actors is present
//-----------------------------------

/**
 * Augment rendered Journal sheets to add secret sections management
 */
 Hooks.on("renderActorSheet", (app, html, data) => {

  // Only for characters linked to a WA article
  if( ! app.object.getFlag("world-anvil", "articleId") ) return;

  journalUpgrade.renderSecretSections(app, html);
  if( game.user.isGM ) {
    journalUpgrade.addToggleSecretsListeners(app, html);
  }
});

